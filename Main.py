print("Loading current courses...")
from initialise import *

courses = CurrentCourses()
print("Current courses loaded!")


def display_help():
    print("C - display current courses")
    print("A - show all active assignments (not implemented)")
    print("N - show new courses / assignments since last visit (not implemented)")
    print("E - exits the program")


print("What do you want to do?")
print("Enter H to display help")
while True:
    option = input().lower()
    if option == "c":
        print(parse_courses(courses.courses))
    elif option == "h":
        display_help()
    elif option == "e":
        break
    else:
        print(option)
