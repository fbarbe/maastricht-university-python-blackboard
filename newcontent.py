from blackboard import *
from datetime import datetime

with open('creds') as f:
    username = f.readline()[:-1]
    password = f.readline()

b = Blackboard(username, password)

def print_leaves(content, from_date=datetime.strptime('2000', '%Y')):
    if content.hasChildren:
        for child in content.children:
            print_leaves(child, from_date=from_date)
    elif type(content) == Content and content.created_datetime() >= from_date:
        print(f"{content.title}")

courses = b.get_current_courses()

try:
    from_date = datetime.strptime(open(".lastaccess", 'r').read(), '%Y-%m-%dT%H:%M')
except:
    open(".lastaccess", 'w').write(datetime.now().strftime('%Y-%m-%dT%H:%M'))
print("New material from last access:")
for course in courses:
    print_leaves(course, from_date=from_date)
    
open(".lastaccess", 'w').write(datetime.now().strftime('%Y-%m-%dT%H:%M'))