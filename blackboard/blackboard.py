import urllib.parse as urlparse
from datetime import datetime
import requests
from bs4 import BeautifulSoup
import re

AUTH_URL = "https://eleum.maastrichtuniversity.nl"
API_ADDRESS = "https://eleum.maastrichtuniversity.nl/learn/api/public/v1/"


class SessionInitializationException(Exception):
    """Raised when a miscommunication results in failure to create session."""


class ConnectionException(Exception):
    """Raised when a miscommunication results in failure to gather content."""


class Blackboard:
    # Based on https://developer.blackboard.com/portal/displayApi
    def __init__(self, username, password):
        """Creates a Session and authenticates for the given user."""

        # This is basically Chris's code for resourcebooker, thanks for that
        self.session = requests.Session()

        response = self.session.get(AUTH_URL)
        if not response.history:  # We expect the first URL to redirect us
            raise SessionInitializationException("Initial response failed to "
                                                 "redirect")
        if "unimaas\\" not in username:
            username = f"unimaas\{username}"

        self.student_id = username[8:]

        parsed = urlparse.urlparse(response.url)
        soup = BeautifulSoup(response.text, 'lxml')
        credentials_url_relpath = soup.find('form').attrs['action']
        credentials_url = urlparse.urlunparse(['https', parsed[1], credentials_url_relpath, '', '', ''])

        credentials = {'UserName': username, 'Password': password, 'AuthMethod': 'FormsAuthentication'}
        response = self.session.post(credentials_url, data=credentials)
        try:
            self.__login_um(response.text)
            self.id = self.get_users({"userName": self.student_id})[0].id
        except:
            raise SessionInitializationException("Your credentials seem to be invalid")

    def __login_um(self, res_text):
        soup = BeautifulSoup(res_text, 'lxml')
        final_auth_url = soup.find('form').attrs['action']
        saml_response = soup.find('input', {'name': 'SAMLResponse'}).attrs['value']
        auth_payload = {'SAMLResponse': saml_response}
        try:
            relay_state = soup.find('input', {'name': 'RelayState'}).attrs[
                'value']  # some websites need relay state, some don't
            auth_payload['RelayState'] = relay_state
        except:
            pass
        self.session.post(final_auth_url, data=auth_payload)  # You're in! All the cookies are now set

    def get_users(self, params: dict = {}):
        """Gets users with certain parameters"""
        # example: get_users({'userName':'i6...'})
        # example: get_users({'name.family':'asdf'})
        response = self.session.get(API_ADDRESS + "users/", params=params).json()
        try:
            return [User(self, x) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_courses(self, params: dict = {}):
        # example: get_users({'name':'Graph Theory'})
        response = self.session.get(API_ADDRESS + "courses/", params=params).json()
        try:
            return [Course(self, x) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_current_courses(self, userid=None):
        if userid == None:
            userid = self.id
        response = self.session.get(API_ADDRESS + f"users/{userid}/courses").json()
        try:
            return [self.get_course(x['courseId']) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_course(self, course_id):
        response = self.session.get(API_ADDRESS + f"courses/{course_id}").json()
        try:
            return Course(self, response)
        except:
            raise ConnectionException(response['message'])

    def get_course_users(self, course_id):
        response = self.session.get(API_ADDRESS + f"courses/{course_id}/users").json()
        try:
            return [User(self, x) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_course_content(self, course_id):
        response = self.session.get(API_ADDRESS + f"courses/{course_id}/contents").json()
        try:
            return [Content(self, x, course_id) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_content(self, course_id, content_id):
        response = self.session.get(API_ADDRESS + f"courses/{course_id}/contents/{content_id}/children").json()
        try:
            return [Content(self, x, course_id) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_content_attachments(self, course_id, content_id):
        response = self.session.get(API_ADDRESS + f"courses/{course_id}/contents/{content_id}/attachments").json()
        try:
            return [Attachment(self, x, course_id, content_id) for x in response['results']]
        except:
            raise ConnectionException(response['message'])

    def get_attachment(self, course_id, content_id, attachment_id):
        response = self.session.get(
            API_ADDRESS + f"courses/{course_id}/contents/{content_id}/attachments/{attachment_id}").json()
        try:
            return Attachment(self, response, course_id, content_id)
        except:
            raise ConnectionException(response['message'])

    def get_collaborate_ultra_token(self, course_id):
        r = self.session.get(
            f'https://eleum.maastrichtuniversity.nl/webapps/collab-ultra/tool/collabultra/lti/launch?course_id={course_id}')
        soup = BeautifulSoup(r.text, 'lxml')
        data = {x.attrs['name']: x.attrs['value'] for x in soup.find_all('input')}
        url = soup.find('form').attrs['action']
        r = self.session.post(url, data=data)
        token = urlparse.unquote(re.findall(r'token=(.*)', r.history[0].headers['Location'])[0])
        return token

    def get_collaborate_ultra_content(self, token, params={}):
        # params:
        # startTime (e.g. 2020-03-14T19:55:54 0100)
        # endTime (e.g 2020-04-13T19:55:54 0200)
        # sort (e.g startTime)
        # order (e.g desc)
        # limit
        # offset
        headers = {'Authorization': f'Bearer {token}'}
        r = self.session.get('https://eu-lti.bbcollab.com/collab/api/csa/recordings', params=params, headers=headers)
        return r.json()

    def get_collaborate_ultra_video_link(self, id):
        return f'https://eu.bbcollab.com/collab/ui/session/playback/load/{id}'

    def get_mediasite(self, mediasite_id):
        # FIXME: This is not 'Blackboard', this is MediaSite, another service in UM
        data = {"getPlayerOptionsRequest": {"ResourceId": f"{mediasite_id}", "QueryString": "?autoStart': 'true",
                                            "UseScreenReader": "false", "UrlReferrer": ""}}
        if 'MediasiteAuth' not in self.session.cookies.keys():  # Check for already existing authentication
            r = self.session.get(
                f'https://mediasite.maastrichtuniversity.nl/mediasite/Play/{mediasite_id}?autoStart=true')
            self.__login_um(r.text)

        r = self.session.post(
            'https://mediasite.maastrichtuniversity.nl/Mediasite/PlayerService/PlayerService.svc/json/GetPlayerOptions',
            json=data)
        return Mediasite(self, r.json(), mediasite_id)


class User:
    def __init__(self, blackboard, json):
        self.__dict__ = json
        self.blackboard = blackboard

    @property
    def courses(self):
        return self.blackboard.get_current_courses(self.id)

    # command line aesthetics
    def __repr__(self):
        return self.__dict__.__repr__()

    def __str__(self):
        return f"<User {self.name} | {self.userName} | {self.id}]"


class Course:
    def __init__(self, blackboard, json):
        self.__dict__ = json
        self.name = json['name']
        self.organisation = json['organization']
        self.blackboard = blackboard
        self.hasChildren = self.availability['available'] == 'Yes' and not self.organization

    def __contains__(self, x):
        return x in self.__dict__

    @property
    def children(self):
        assert self.hasChildren, "This content does not have children"
        return self.blackboard.get_course_content(self.id)

    def __str__(self):
        return f"<Course {self.name} | {self.id}>"

    # command line aesthetics
    def __repr__(self):
        return self.__dict__.__repr__()


class Content:
    def __init__(self, blackboard, json, course_id):
        self.__dict__ = json
        self.course_id = course_id
        self.blackboard = blackboard
        if not hasattr(self, 'hasChildren'):
            self.hasChildren = False

    def __contains__(self, x):
        return x in self.__dict__

    @property
    def children(self):
        assert self.hasChildren, "This content does not have children"
        return self.blackboard.get_content(self.course_id, self.id)

    def created_datetime(self):
        return datetime.strptime(self.created[:19], "%Y-%m-%dT%H:%M:%S")

    def has_attachments(self):
        return not self.hasChildren and not 'mediasite' in self.contentHandler['id']

    def has_mediasite(self):
        return 'mediasite' in self.contentHandler['id']

    @property
    def mediasite(self):
        assert self.has_mediasite(), "This content does not have Mediasite attached to it"
        presentation_id = re.findall(r'presentation_id=([^&]*)&', self.body)[0]
        return self.blackboard.get_mediasite(presentation_id)

    @property
    def attachments(self):
        return self.blackboard.get_content_attachments(self.course_id, self.id)

    # command line aesthetics
    def __repr__(self):
        return self.__dict__.__repr__()

    def __str__(self):
        return f"<Content {self.title} | {self.id} | course {self.course_id}>"


class Attachment:
    def __init__(self, blackboard, json, course_id, content_id):
        self.__dict__ = json
        self.content_id = content_id
        self.course_id = course_id
        self.blackboard = blackboard

    def download_link(self):
        return f"{API_ADDRESS}courses/{self.course_id}/contents/{self.content_id}/attachments/{self.id}/download"

    def download(self):
        return self.blackboard.session.get(self.download_link()).content

    # command line aesthetics
    def __repr__(self):
        return self.__dict__.__repr__()

    def __str__(self):
        return f"<Attachment {self.fileName} | {self.id} | content {self.content_id}>"


class Mediasite:
    # FIXME: This is not 'Blackboard', this is MediaSite, another service in UM
    def __init__(self, blackboard, json, id):
        self.mediasite_id = id
        self.__dict__ = json
        self.url = json['d']['Presentation']['Streams'][0]['VideoUrls'][0]['Location']
        self.id = re.findall(r'MP4Video\/(.*)\.mp4', self.url)[0]

    def download(self):
        return requests.get(
            f'https://mediasite-media.maastrichtuniversity.nl/MediasiteDeliver/OnDemand/MP4Video/{self.id}.mp4/QualityLevels(4496000)').content
